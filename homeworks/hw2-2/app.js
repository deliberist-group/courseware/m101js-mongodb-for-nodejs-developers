var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/weather', function(err, db) {
    if(err) throw err;

    var current_state = "balls ... initial state ... balls";

    var options = { 'sort' : [['State', 1], ['Temperature', -1]] };

    var data = db.collection('data');
    var cursor = data.find({}, {}, options);

    cursor.each(function(err, doc) {
        if(err) throw err;

        if(doc == null) {
            console.log('No more documents found for query.');
            return db.close();
        }

        if (current_state != doc['State']) {
            current_state = doc['State'];
            doc['month_high'] = true;
            console.log('Set the monthly high for: ' + JSON.stringify(doc));
        }

        var update_query = { 
            '_id' : doc['_id']
        };

        db.collection('data').update(update_query, doc, function(err, updated) {
            if(err) throw err;

            console.dir('Successfully updated document: ' + doc['_id']);
        });

    });

});

