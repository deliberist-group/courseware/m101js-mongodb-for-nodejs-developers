var bcrypt = require('bcrypt-nodejs');

/* The UsersDAO must be constructed with a connected database object */
function UsersDAO(db) {
    "use strict";

    /* If this constructor is called without the "new" operator, "this" points
     * to the global object. Log a warning and call it correctly. */
    if (false === (this instanceof UsersDAO)) {
        console.log('Warning: UsersDAO constructor called without "new" operator');
        return new UsersDAO(db);
    }

    var users = db.collection("users");

    this.addUser = function(username, password, email, callback) {
        "use strict";

        // Generate password hash
        var salt = bcrypt.genSaltSync();
        var password_hash = bcrypt.hashSync(password, salt);

        // Create user document
        var user = {'_id': username, 'password': password_hash};

        // Add email if set
        if (email != "") {
            user['email'] = email;
        }

        // TODO: hw2.3
        //callback(Error("addUser Not Yet Implemented!"), new_user);

        // HOMEWORK 2.3
        users.insert(user, function(err, inserted) {
            // The inserted document is actually an array.  Just take the first
            // document in the array.
            if (!err) console.dir("Successfully inserted: " + JSON.stringify(inserted[0]));
            callback(err, inserted[0]);
        });
        // END OF HOMEWORK 2.3
    }

    this.validateLogin = function(username, password, callback) {
        "use strict";

        // Callback to pass to MongoDB that validates a user document
        function validateUserDoc(err, user) {
            "use strict";

            if (err) return callback(err, null);

            if (user) {
                if (bcrypt.compareSync(password, user.password)) {
                    callback(null, user);
                }
                else {
                    var invalid_password_error = new Error("Invalid password");
                    // Set an extra field so we can distinguish this from a db error
                    invalid_password_error.invalid_password = true;
                    callback(invalid_password_error, null);
                }
            }
            else {
                var no_such_user_error = new Error("User: " + user + " does not exist");
                // Set an extra field so we can distinguish this from a db error
                no_such_user_error.no_such_user = true;
                callback(no_such_user_error, null);
            }
        }

        // TODO: hw2.3
        //callback(Error("validateLogin Not Yet Implemented!"), null);

        // HOMEWORK 2.3
        var query = { '_id' : username };
        users.find(query).toArray(function(err, docs) {
            // The returned document is either an error or an array, just take 
            // the first element in the array.
            if (err)    validateUserDoc(err, null);
            else        validateUserDoc(null, docs[0]);
        });
        // END OF HOMEWORK 2.3
    }
}

module.exports.UsersDAO = UsersDAO;
